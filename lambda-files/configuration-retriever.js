const AWS = require('aws-sdk');

const s3 = new AWS.S3();

module.exports = class ConfigurationRetriever {
  constructor(bucket, key) {
    this.bucket = bucket;
    this.key = key;
  }

  async retrieve() {
    console.log(`Reading configuration at ${this.bucket}/${this.key}`);
    const params = {
      Bucket: this.bucket,
      Key: this.key,
    };

    return s3.getObject(params)
      .promise()
      .then(response => JSON.parse(response.Body))
      .catch((error) => {
        console.log(`Failed to config from S3: ${error}`);
        throw error;
      });
  }
};
