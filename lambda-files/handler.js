// TODO move to config file
const bucket = 'campsite-availability';
const snsTopicArn = 'arn:aws:sns:us-east-1:342769519861:campsiteAvailability';

const ConfigurationRetriever = require('./configuration-retriever.js');
const AvailabilityStore = require('./availability-store.js');
const AvailabilityNotifier = require('./availability-notifier.js');
const AvailabilityRetriever = require('./availability-retriever.js');

const availabilityStore = new AvailabilityStore(bucket);
const availabilityNotifier = new AvailabilityNotifier(snsTopicArn);
const availabilityRetriever = new AvailabilityRetriever();

async function checkSite(campsite) {
  const array = await Promise.all([
    availabilityRetriever.retrieveAvailability(campsite),
    availabilityStore.getAvailability(campsite.name)]);

  const isAvailable = array[0];
  const wasAvailable = array[1];

  if (isAvailable !== wasAvailable) {
    console.log(`${campsite.name}: Notifying because status has changed. Previous = ${wasAvailable}, current = ${isAvailable}`);
    return Promise.all([
      availabilityNotifier.notify(isAvailable, campsite),
      availabilityStore.setAvailability(isAvailable, campsite.name),
    ]);
  }

  return Promise.resolve();
}

async function doChecks() {
  const configurationRetriever = new ConfigurationRetriever(bucket, 'campground_config_v3.json');
  const configuration = await configurationRetriever.retrieve();
  return Promise.all(configuration.campsites.map(campsite => checkSite(campsite)));
}

module.exports.campcheck = function(event, context) {
  doChecks()
    .then(() => {
      console.log('Finished successfully');
      context.succeed();
    })
    .catch((error) => {
      console.error(`Finished unsuccessfully: ${error}\n${error.stack}`);
      context.fail(error);
    });
};
