const request = require('request-promise-native');

async function getResponses(monthStartDates, siteInfo) {
  const map = new Map();
  const requestPairsPromise = monthStartDates.map((monthStartDate) => {
    const address = `https://www.recreation.gov/api/camps/availability/campground/${siteInfo.id
    }/month?start_date=${monthStartDate}T00%3A00%3A00.000Z`;

    console.log(`${siteInfo.name}: Calling ${address}`);
    return Promise.all([monthStartDate, request(address)]);
  });

  // Resolve this way in order to take advantage of parallel http calls
  const requestPairs = await Promise.all(requestPairsPromise);

  requestPairs.forEach(values => map.set(values[0], values[1]));

  return map;
}

function getMonthStartFromDate(date) {
  return date.replace(/(\d+)-(\d+)-(\d+)/, '$1-$2-01');
}

module.exports = class AvailabilityRetriever {
  async retrieveAvailability(siteInfo) {
    console.log(`${siteInfo.name}: Checking availability for dates:`);
    console.dir(siteInfo.dates);
    console.log('And campsite types:');
    console.dir(siteInfo.types);
    const monthStartDates = Array.from(new Set(siteInfo.dates.map(
      date => getMonthStartFromDate(date)
    )));

    try {
      const responseMap = await getResponses(monthStartDates, siteInfo);

      // Convert response bodies to JSON
      responseMap.forEach((value, key, map) => map.set(key, JSON.parse(value).campsites));

      // Assume all months have same campsite list
      const availabilities = responseMap.get(monthStartDates[0]);

      // Check each campsite
      for (const key in availabilities) {
        if (Object.prototype.hasOwnProperty.call(availabilities, key)) {
          const availability = availabilities[key];
          const typeMatches = Object.prototype.hasOwnProperty.call(siteInfo, 'types')
            ? siteInfo.types.includes(availability.campsite_type)
            : true;

          if (typeMatches) {

            // Make sure all dates are available for this campsite
            let isAvailable = true;
            for (let i = 0; i < siteInfo.dates.length; i += 1) {
              // Make sure to check the correct response based on the date
              const currentDate = getMonthStartFromDate(siteInfo.dates[i]);
              const availabilityForMonth = responseMap.get(currentDate)[key];
              if (availabilityForMonth.availabilities[`${siteInfo.dates[i]}T00:00:00Z`] !== 'Available') {
                isAvailable = false;
                break;
              }
            }

            if (isAvailable) {
              console.log(`${siteInfo.name}: Found availability at loop ${availability.loop}, site ${availability.site}, type ${availability.campsite_type}`);
              return isAvailable;
            }
          }
        }
      }

      return false;
    } catch (error) {
      console.error('error:', error);
      throw error;
    }
  }
};
