const AWS = require('aws-sdk');

const sns = new AWS.SNS();

module.exports = class AvailabilityNotifier {
  constructor(topicArn) {
    this.topicArn = topicArn;
  }

  async notify(isAvailable, siteInfo) {
    AWS.config.update({
      accessKeyId: 'AKIAJ42IWRQJIA4XWHCQ',
      secretAccessKey: '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',
      region: 'us-east-1',
    });

    let publishParams;
    if (isAvailable) {
      publishParams = {
        TopicArn: this.topicArn,
        Subject: `A campsite just became available at ${siteInfo.name}!`,
        Message: `A campsite just became available at ${siteInfo.name}!\nPlease visit tinyurl.com/y8mz324m ASAP to book!\n As a reminder, the dates are ${siteInfo.dates}`,
      };
    } else {
      publishParams = {
        TopicArn: this.topicArn,
        Subject: `Campsites at ${siteInfo.Name} are no longer available`,
        Message: `Previously available campsites at ${siteInfo.name} are no longer available. You will receive an email if this changes.\nAs a reminder, the dates are ${siteInfo.dates}`,
      };
    }

    console.log(`${siteInfo.name}: Attempting to publish to SNS`);

    return sns.publish(publishParams)
      .promise()
      .then(() => console.log(`${siteInfo.name}: Succeeded to publish to SNS`))
      .catch((error) => {
        console.error(`${siteInfo.name}: Failed to publish to SNS: ${error}`);
        throw error;
      });
  }
};
