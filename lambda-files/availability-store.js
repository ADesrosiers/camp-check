const AWS = require('aws-sdk');

const s3 = new AWS.S3();

module.exports = class AvailabilityStore {
  constructor(bucket) {
    this.bucket = bucket;
  }

  async setAvailability(isAvailable, siteName) {
    console.log(`${siteName}: Updating availability in S3`);
    const body = `{\n  "notified" : ${isAvailable}\n}`;

    const config = {
      Bucket: this.bucket,
      Key: `${siteName}.json`,
      Body: body,
    };

    try {
      await s3.putObject(config).promise();
    } catch (error) {
      console.log(`${siteName}: Failed to update availability in S3: ${error}`);
      throw error;
    }
  }

  async getAvailability(siteName) {
    console.log(`${siteName}: Checking previous availability`);
    const params = {
      Bucket: this.bucket,
      Key: `${siteName}.json`,
    };

    try {
      await s3.headObject(params).promise();
    } catch (error) {
      if (error.code === 'NotFound') {
        console.log('Creating status file because it does not exist');
        this.setAvailability(false, siteName);
        return false;
      }
      console.error(`${siteName}: Failed to get object metadata ${error}`);
    }

    try {
      const data = await s3.getObject(params).promise();
      const obj = JSON.parse(data.Body);
      return obj.notified;
    } catch (error) {
      console.log(`${siteName}: Failed to read availability in S3: ${error}`);
      throw error;
    }
  }
};
